FROM openjdk:17-jdk-alpine

# Install wget
RUN apk add --no-cache wget
RUN apk add --no-cache docker

# Install Maven manual way
RUN wget https://dlcdn.apache.org/maven/maven-3/3.9.4/binaries/apache-maven-3.9.4-bin.tar.gz -P /tmp \
    && tar xf /tmp/apache-maven-3.9.4-bin.tar.gz -C /opt \
    && ln -s /opt/apache-maven-3.9.4 /opt/maven

# Set environment variables
ENV M2_HOME=/opt/maven
ENV MAVEN_HOME=/opt/maven
ENV PATH=${M2_HOME}/bin:${PATH}

# Install Docker Compose standalone
ARG DOCKER_COMPOSE_VERSION=2.2.3
RUN wget https://github.com/docker/compose/releases/download/v${DOCKER_COMPOSE_VERSION}/docker-compose-linux-x86_64 -O /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

CMD ["sh", "-c", "dockerd &> /dev/null & sleep 5 && docker info"]
